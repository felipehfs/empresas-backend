
exports.up = function(knex) {
  return knex.schema.createTable('profile', (table) => {
      table.uuid('id').primary()
      table.string("city").notNullable();
      table.string("country").notNullable();
      table.string("photo");
      table.boolean("first_access").defaultTo(false);
      table.boolean('super_angel').defaultTo(false);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('profile');
};
