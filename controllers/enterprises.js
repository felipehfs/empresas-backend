const knex = require('../config/knex');

module.exports = {
    async findAll(req, res) {
        try {
            const query = {};
            if (req.headers('uid') !== req.uid || req.headers('client') !== req.client) {
                return res.status(401).send("Unauthorizated");
            }
            if (req.query.name) query.enterprise_name = req.query.name;
            if (req.query.type) query.type = req.query.type;
            
            let enterprises = null;
            
            if (Object.keys(query).length > 0) {
                enterprises = await knex("enterprise").select().where(query);
                return res.json(enterprises);
            } else {
                enterprises = await knex("enterprise").select();
            }
            return res.json(enterprises);
        } catch(e) {
            console.log(e);
            return res.status(500).json({ error: e});
        }
    },
    async details(req, res) {
        try {
            if (req.headers('uid') !== req.uid || req.headers('client') !== req.client) {
                return res.status(401).send("Unauthorizated");
            }
            const enterprise = await knex("enterprise").select().where({ id: req.params.id});
            return res.json(enterprise[0]? enterprise[0]: null);
        } catch(e) {
            console.log(e);
            return res.status(500).json({ error: e});   
        }
    }

}