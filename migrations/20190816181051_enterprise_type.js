
exports.up = function(knex) {
  return knex.schema.createTable('enterprise_type', table => {
      table.uuid('id').primary();
      table.string('enterprise_type_name').notNullable();
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('enterprise_type');
};
