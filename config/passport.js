const passport = require('passport');
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');
const knex = require('./knex');

function verify(jwtPayload, callback) {
    knex('users').select().where({ id: jwtPayload.id, email: jwtPayload.uid })
    .then((user) => callback(null, user[0]))
    .catch(err => callback(err));
}

passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'b330cf2425c6ac1561c63f825e680a53'
}, verify));

module.exports = {
    initialize() {
        return passport.initialize();
    },
    authenticate() {
        return passport.authenticate('jwt', { session: false});
    }
}