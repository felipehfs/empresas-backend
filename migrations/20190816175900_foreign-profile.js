
exports.up = function(knex) {
    return knex.schema.table('profile', table => {
        table.uuid('user_id').references('id').inTable('users').onDelete('cascade');
    });
};

exports.down = function(knex) {
  return knex.schema.table('profile', table => {
      table.dropColumn('user_id');
  });
};
