const faker = require("faker");

const createEnterprise = () => ({
  id: faker.random.uuid(),
  email_enterprise: faker.internet.email(),
  enterprise_name: faker.company.companyName(),
  description: faker.lorem.words(10),
  city: faker.address.city(),
  country: faker.address.country(),
  value: faker.random.number(200),
  facebook: faker.internet.userName(),
  linkedin: faker.internet.userName()
});
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  const data = []

  for (let i = 0; i < 100; i++) {
      data.push(createEnterprise())
  }
    // Inserts seed entries
  await knex('enterprise').insert(data);
};
