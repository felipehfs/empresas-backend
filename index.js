const express = require("express");
const app = express();
const morgan = require('morgan');
const passport = require('./config/passport')
const bodyParser = require("body-parser");
const routes = require('./routes');
const PORT = 8081;

app.use(morgan('common'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(passport.initialize());

app.use(routes);
app.listen(PORT, () => console.log(`Running http://localhost:${PORT}`));