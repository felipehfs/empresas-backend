const dev = process.env.environment || 'development';
const config = require('../knexfile')[dev];
const knex = require("knex")(config);

module.exports = knex;