const knex = require('../config/knex');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const uuid = require('uuid/v4');

module.exports = {
    async signout(req, res) {
        try {
            const password = await bcrypt.hash(req.body.password, 10);
            const newUser = {
                id: uuid(), 
                name: req.body.name, 
                email: req.body.email,
                password
            };
            await knex('users').insert(newUser);
            newUser.password = undefined;
            return res.json(newUser);
        }catch(e) {
            console.log(e);
            return res.status(500).json({error: e});
        }
    },
    async login(req, res) {
        try {
            const user = await knex("users").select().where({ email: req.body.email});
            if (!user[0]) {
                return res.status(404).json({ err: "user not found"});
            }
            
            const isEquals = await bcrypt.compare(req.body.password, user[0].password);
            if (isEquals) {
                return res.status(400).json({ error: 'Wrong email or password'});
            }
            
            const payload = {
                id: user[0].id,
                uid: user[0].email,
                client: user[0].name
            }
            const token = jwt.sign(payload, 'b330cf2425c6ac1561c63f825e680a53', { expiresIn: '7d'});
            return res.json({
                accessToken: token,
                ...payload
            });
        } catch(e) {
            console.log(e);
            return res.status(500).json({ error: e});   
        }
    }

}