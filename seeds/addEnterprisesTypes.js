const faker = require('faker');

const createEnterprise = () => ({
  id: faker.random.uuid(),
  enterprise_type_name: faker.random.words()
});

exports.seed = async function(knex) {
  // Deletes ALL existing entries
  const data = Array(7).fill(createEnterprise());
  console.log(data)
  await knex('enterprise_type').insert(data);
};
