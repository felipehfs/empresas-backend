
exports.up = function(knex) {
    return knex.schema.createTable('enterprise', (table) => {
        table.uuid('id').primary();
        table.string("enterprise_name").notNullable();
        table.string("email_enterprise").notNullable();
        table.string("description").notNullable();
        table.string("facebook");
        table.string("twitter");
        table.string("linkedin");
        table.string("photo");
        table.string("phone");
        table.string("city");
        table.string("country");
        table.decimal("share_price");
        table.decimal("value");
    });
};

exports.down = function(knex) {
  return knex.schema.dropTable('enterprise');
};
