const express = require("express");
const router = express.Router();
const passport = require("passport");
const enterprise = require('../controllers/enterprises');
const user = require('../controllers/user');

router.post('/api/signout', user.signout);
router.post('/api/login', user.login);

router.get('/api/enterprises', passport.authenticate('jwt',{ session: false}), enterprise.findAll);
router.get('/api/enterprises/:id', passport.authenticate('jwt',{ session: false}), enterprise.details);

module.exports = router;