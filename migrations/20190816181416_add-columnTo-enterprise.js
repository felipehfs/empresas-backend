
exports.up = function(knex) {
    return knex.schema.table('enterprise', table => {
        table.uuid('enterprise_type').references('id').inTable('enterprise_type').onDelete('cascade');
    });
};

exports.down = function(knex) {
    return knex.schema.table('enterprise', table => {
        table.dropColumn('enterprise_type');
    });
};
